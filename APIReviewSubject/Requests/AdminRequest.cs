﻿namespace APIReviewSubject.Requests
{
    public class AdminRequest
    {
        public string userName { get; set; }
        public string password { get; set; }

        public AdminRequest() { }
    }
}
